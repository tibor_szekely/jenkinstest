package service;

import domain.Grade;
import domain.Homework;
import domain.Student;
import repository.GradeXMLRepository;
import repository.HomeworkXMLRepository;
import repository.StudentXMLRepository;
import validation.*;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class ServiceTest {

    public static Service service;

    public static Validator<Student> studentValidator;
    public static Validator<Homework> homeworkValidator;
    public static Validator<Grade> gradeValidator;

    @org.junit.jupiter.api.BeforeAll
    public static void setUp() {
        studentValidator = new StudentValidator();
        homeworkValidator = new HomeworkValidator();
        gradeValidator = new GradeValidator();

        StudentXMLRepository fileRepository1 = new StudentXMLRepository(studentValidator, "students.xml");
        HomeworkXMLRepository fileRepository2 = new HomeworkXMLRepository(homeworkValidator, "homework.xml");
        GradeXMLRepository fileRepository3 = new GradeXMLRepository(gradeValidator, "grades.xml");

        service = new Service(fileRepository1, fileRepository2, fileRepository3);
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Test
    void findAllStudents() {
    }

    @org.junit.jupiter.api.Test
    void findAllHomework() {
        Iterable<Homework> hws = service.findAllHomework();
        int initialLength;
        if (hws instanceof Collection) {
            initialLength = ((Collection<?>) hws).size();
        } else {
            int counter = 0;
            for (Object i : hws) {
                counter++;
            }
            initialLength = counter;
        }
        Homework hw = new Homework("46","VVSS",13,2);
        int result = service.saveHomework(hw.getID(),hw.getDescription(),hw.getDeadline(), hw.getStartline());
        Iterable<Homework> hws2 = service.findAllHomework();
        int finalLength;
        if (hws2 instanceof Collection) {
            finalLength = ((Collection<?>) hws2).size();
        } else {
            int counter = 0;
            for (Object i : hws2) {
                counter++;
            }
            finalLength = counter;
        }
        assertTrue(finalLength == initialLength + 1);
        service.deleteHomework(hw.getID());
    }

    @org.junit.jupiter.api.Test
    void findAllGrades() {
    }

    @org.junit.jupiter.api.Test
    void saveValidStudent() {
        Student st = new Student("44", "Tibor", 222);
        int result = service.saveStudent(st.getID(), st.getName(), st.getGroup());
        assertTrue(result == 1);
        service.deleteStudent(st.getID());
    }

    @org.junit.jupiter.api.Test
    void saveValidHomework() {
        Homework hw = new Homework("43","VVSS hw",6,2);
        int result = service.saveHomework(hw.getID(),hw.getDescription(),hw.getDeadline(), hw.getStartline());
        assertTrue(result == 1);
        service.deleteHomework(hw.getID());
    }

    @org.junit.jupiter.api.Test
    void saveInvalidStudent() {
        Student st = new Student("55", "Szekely", 10);
        int result = service.saveStudent(st.getID(), st.getName(), st.getGroup());
        Exception exception = assertThrows(ValidationException.class, () -> {
            studentValidator.validate(st);
        });
        String expectedMessage = "Group invalid! \n";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

        assertTrue(result == 1);
        service.deleteStudent(st.getID());
    }

    @org.junit.jupiter.api.Test
    void saveInvalidHomework() {
        Homework hw = new Homework("42","VVSS",13,0);
        int result = service.saveHomework(hw.getID(),hw.getDescription(),hw.getDeadline(), hw.getStartline());
        Exception exception = assertThrows(ValidationException.class, () -> {
            homeworkValidator.validate(hw);
        });
        String expectedMessage = "Date received is invalid! \n";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

        assertTrue(result == 1);
        service.deleteHomework(hw.getID());
    }

    @org.junit.jupiter.api.Test
    void saveGrade() {
    }

    @org.junit.jupiter.api.Test
    void deleteStudent() {
        int result = service.deleteStudent("9999");
        assertEquals(0, result);

        int result2 = service.deleteStudent("1");
        assertEquals(1, result2);
        Student st = new Student("1", "Ana", 221);
        service.saveStudent(st.getID(), st.getName(), st.getGroup());
    }

    @org.junit.jupiter.api.Test
    void deleteHomework() {
        int result = service.deleteHomework("9999");
        assertEquals(0, result);

        int result2 = service.deleteHomework("1");
        assertEquals(1, result2);
        Homework hw = new Homework("1","File",7,6);
        service.saveHomework(hw.getID(),hw.getDescription(),hw.getDeadline(), hw.getStartline());
    }

    @org.junit.jupiter.api.Test
    void updateStudent() {
    }

    @org.junit.jupiter.api.Test
    void updateHomework() {
        Homework hw = new Homework("9999","New",7,6);
        int result = service.updateHomework(hw.getID(),hw.getDescription(),hw.getDeadline(), hw.getStartline());
        //assertNotEquals(1, result);

        Homework hw2 = new Homework("1","File",7,6);
        int result2 = service.updateHomework(hw2.getID(),hw2.getDescription(),hw2.getDeadline(), hw2.getStartline());
        //assertFalse(result2 == 0);

        assertAll(
                () -> assertNotEquals(1, result),
                () -> assertFalse(result2 == 0)
        );
    }

    @org.junit.jupiter.api.Test
    void extendDeadlineValid() {
        int result = service.extendDeadline("1", 8);
        assertTrue(result == 1);
    }

    @org.junit.jupiter.api.Test
    void extendDeadlineInvalid() {
        int result = service.extendDeadline("2", -2);
        assertTrue(result == 0);
    }

    @org.junit.jupiter.api.Test
    void createStudentFile() {
    }
}